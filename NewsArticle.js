import { LitElement, html, css } from 'https://unpkg.com/lit-element?module';

export class NewsArticle extends LitElement {
    
    static get properties() {
        return {
            title: {type: String},
            description: {type: String},
            urlToImage: {type: String},
            read: {type: Boolean}
        };
    }

    static get styles() {
        return css`
        
        :host {
            display: block;
        }

        .container {
            position: relative;
            width: 70%;
            margin: 15px auto;
            box-shadow: 5px 5px 9px 5px #898989;
        }

        p {
            padding: 5px;
        }

        img {
            display: block;
            margin: 10px auto;
        }

        .status-label {
            position: absolute;
            top: 2px;
            right: 2px;
            font-size: 15px;
            border: 2px solid gray;
            border-radius: 3px;
            padding: 5px;
        }

        .status-label.unread {
            color: red;
        }

        .status-label.read {
            color: blue;
        }
        `;
    }
    
    constructor() {
        super();
    }

    _toggleReadStatus() {
        this.read = !this.read;
        this.dispatchEvent(new CustomEvent('read-status-changed', {detail: {read: this.read} }));
    }

    render() {
        return html`
            <div class="container">
                <div class="status-label ${statusClass(this.read)}" @click=${this._toggleReadStatus}>${readStatus(this.read)}</div>
                <header>
                    <h2>${this.title}</h2>
                </header>
                <main>
                    <p>${this.description}</p>
                </main>
                <img src="${this.urlToImage}" height="300px"/>
            </div>
        `;
    }
}

function readStatus(read) {
    if (read) {
        return 'Read';
    } else {
        return 'Unread';
    }
}

function statusClass(read) {
    if (read) {
        return 'read';
    } else {
        return 'unread';
    }
}

customElements.define('news-article', NewsArticle);