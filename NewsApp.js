import { LitElement, html, css } from 'https://unpkg.com/lit-element?module';
import { NewsArticle } from './NewsArticle.js';

class NewsApp extends LitElement {
    
    //943470f72ce24788839783a3c24e41ef API key

    static get properties() {
        return {
            articles: {type: Array},
            loading: {type: Boolean},
            filter: {type: String}
        };
    }

    static get styles() {
        return css`
        :host {
            display: block;
        }

        h1 {
            color: green;
            text-align: center;
        }
        
        .loader {
            width: 120px;
            height: 120px;
            border: 16px solid blue;
            border-bottom-color: orange;
            border-top-color: orange;
            border-radius: 50%;
            animation: spin 2s linear infinite;
            margin: 10px auto;
        }
        
        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        ul {
            list-style: none;
        }

        .stats {
            margin: 10px auto;
            width: 200px;
        }

        .filters-container {
            width: 200px;
            margin: 20px auto;
        }

        button {
            background-color: grey;
            border: 1px solid black;
            border-radius: 3px;
            padding: 5px 10px;
            margin: 0 5px;
            color: white;
        }
        `;
    }

    header = html`<h1>News App</h1>`;
    loader = html`
    <div class="loader"></div>
    `;


    constructor() {
        super();
        this.filter = FILTERS.ALL;
    }

    connectedCallback() {
        super.connectedCallback();

        if (!this.articles) {
            this.fetchArticles();
        }
    }

    async fetchArticles() {
        this.loading = true;
        const response = await fetch('https://newsapi.org/v2/everything?q=web%20development&apiKey=943470f72ce24788839783a3c24e41ef');
        const jsonResponse = await response.json();
        this.articles = jsonResponse.articles;
        this.loading = false;
    }

    _display(filter) {
        this.filter = filter;
    }

    render() {
        if (this.loading) {
            return html`
            ${this.header}
            ${this.loader}
            `;
        }

        const readCount = this.articles.filter(article => article.read).length;
        const unreadCount = this.articles.length - readCount;

        let toRender;
        switch (this.filter) {
            case FILTERS.ALL:
                toRender = this.articles;
                break;
            case FILTERS.READ:
                toRender = this.articles.filter(article => article.read);
                break;
            case FILTERS.UNREAD:
                toRender = this.articles.filter(article => !article.read);
                break;
        }

        return html`
            ${this.header}
            <div class="stats">
            <div> Numer of read articles: ${readCount}</div>
            <div> Numer of unread articles: ${unreadCount}</div>
            </div>
            <div class="filters-container">
                <button @click=${() => this._display(FILTERS.ALL)}>All</button>
                <button @click=${() => this._display(FILTERS.READ)}>Read</button>
                <button @click=${() => this._display(FILTERS.UNREAD)}>Unread</button>
            </div>
            <ul>
            ${toRender.map(article => html`
            <li>
            <news-article 
            .title=${article.title} 
            .description=${article.description} 
            .urlToImage=${article.urlToImage}
            .read=${article.read}
            @read-status-changed=${(e) => this._updateReadStatus(e, article)}
            ></news-article>
            </li>
            `)}
            </ul>
        `;
    }

    _updateReadStatus(e, articleToUpdate) {
        this.articles = this.articles.map( article => {
            return article === articleToUpdate ? {...article, read: e.detail.read} : article;
        });
    }
}

const FILTERS = {
    READ: 'READ',
    UNREAD: 'UNREAD',
    ALL: 'ALL'
}

customElements.define('news-app', NewsApp);